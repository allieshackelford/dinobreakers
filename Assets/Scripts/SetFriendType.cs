﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFriendType : MonoBehaviour
{
    public int _dinoIndex;

    private void Awake()
    {
        // create singleton
        Singleton();
    }

    private void Singleton()
    {
        // check how many objects exist
        int numberObjs = FindObjectsOfType<SetFriendType>().Length;
        if (numberObjs > 1) // if too many, destroy object
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }


    // ---------SETTER FUNCTIONS FOR _dinoIndex --------------------
    public void SetBlue() {
        _dinoIndex = 0;
    }

    public void SetGreen()
    {
        _dinoIndex = 1;
    }

    public void SetYellow()
    {
        _dinoIndex = 2;
    }

}
