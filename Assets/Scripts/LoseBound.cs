﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseBound : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        // if main ball hits lose collider, minus 1 life
        if(other.gameObject.tag == "Ball"){
            Debug.Log("ball hit");
            FindObjectOfType<Ball>().lives--;
        }
    }
}
