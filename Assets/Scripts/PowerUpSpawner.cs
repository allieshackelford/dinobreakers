﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    // powerup to create
    [SerializeField] private GameObject _powerup;

    private void Start()
    {
        StartCoroutine(CreatePowerUp());
    }

    private IEnumerator CreatePowerUp()
    {
        while (true)
        {
            // create a powerup at spawner every 8 sec
            yield return new WaitForSeconds(8f);        // should be in sync with friendAI attack animation
            Instantiate(_powerup, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
        }
    }
}
