﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBall : MonoBehaviour
{
    // powered ball rigidbody
    private Rigidbody2D _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        // set random velocity
        _rigidbody.velocity = new Vector2(Random.Range(2f, 5f), Random.Range(10f, 15f));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // change velocity randomly for each bounce
        Vector2 velocityChange = new Vector2(Random.Range(0, .2f), Random.Range(0, .2f));
        _rigidbody.velocity += velocityChange;
    }
}
