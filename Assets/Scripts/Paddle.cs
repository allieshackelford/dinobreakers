﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    // user input 
    private Vector2 _input;
    // snappy player movement
    private float _directionChange;
    // paddle kinetic rigidbody
    private Rigidbody2D _rigidbody;
    // paddle move speed
    [SerializeField] private float _moveSpeed = 10f;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // move paddle with user input, only along horizontal
        Move();
        _rigidbody.velocity = _input * _moveSpeed;
    }

    private void Move(){
        // set new velocity
        _directionChange = Input.GetAxisRaw("Horizontal");
        _input = new Vector2(_directionChange, 0);
    }
}
