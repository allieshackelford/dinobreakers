﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyAI : MonoBehaviour
{
    public Animator _animator;  // friend dino animator
    private float _waitTime = 8f;   // wait time between animation changes
    public bool _dead = false;      // keep track of if player has lost

    private void Start()
    {
        _animator = GetComponent<Animator>();
        // start throw powerup coroutine
        StartCoroutine(ThrowPowerUp());
    }

    private IEnumerator ThrowPowerUp()
    {
        while (!_dead)
        { 
            // check if player has lost
            if(FindObjectOfType<Ball>().lives <= 0){
                _dead = true;
                break;
            }
            // wait for wait time before playing animation again
            yield return new WaitForSeconds(_waitTime);
            // play attack animation for 3 seconds
            _animator.Play("attack");
            if(FindObjectOfType<Ball>().lives <= 0){
                _dead = true;
                break;
            }
            yield return new WaitForSeconds(3f);
            // go back to idle animation
            _animator.Play("idle");
            _waitTime = 5f;
            if(FindObjectOfType<Ball>().lives <= 0){
                _dead = true;
                break;
            }
        }
        // play damage animation
        Debug.Log("damage animation");
        _animator.Play("damage");
        yield return new WaitForSeconds(2.5f);
    }
}
