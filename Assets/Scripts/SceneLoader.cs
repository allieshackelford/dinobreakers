﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    // load main game scene
    public void LoadGame()
    {
        SceneManager.LoadScene("Gameplay");
    }
    // load main menu
    public void LoadStartMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    // load game over
    public void LoadGameOver()
    {
        StartCoroutine(DelayScene());
    }
    // quit game
    public void QuitGame()
    {
        Application.Quit();
    }

    private IEnumerator DelayScene()
    {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("GameOver");
    }
}
