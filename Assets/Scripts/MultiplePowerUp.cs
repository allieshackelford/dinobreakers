﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplePowerUp : MonoBehaviour
{
    // prefab to instantiate
    [SerializeField] private GameObject _ball;


    private void Start()
    {
        // destroy after 6 sec lifetime
        Destroy(gameObject, 6f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // if ball hits powerup
        if (collision.gameObject.tag == "Ball")
        {
            for (int i = 0; i < 4; i++)
            {
                // create ball and set velocity randomly
                Instantiate(_ball, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
            }
            // destroy powerup
            Destroy(gameObject);
        }
    }
}
