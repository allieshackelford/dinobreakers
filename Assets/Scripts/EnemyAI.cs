﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    // patrol points
    [SerializeField] private Transform[] _points;

    // blocks to look after
    [SerializeField] private Block[] _blocks;

    // timer for enemy movement
    private float _timer = 0;
    // duration of patrol
    private float _duration = 3f;

    //check if enemy has died
    public bool _isDead = false;
    
    // rigidbody
    private Rigidbody2D _rigidbody;

    // animator
    private Animator _animator;

    // starting facing direciton
    private bool _direction;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _direction = GetComponent<SpriteRenderer>().flipX;
        StartCoroutine(Patrol());   // start patrolling on start
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if hit by ball or superball, set is dead to true and call death coroutine
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Superball")
        {
            _isDead = true;
            StartCoroutine(Death());
        }
    }

    private IEnumerator Death()
    {
        // play enemy damage animation for 1 second
        _animator.Play("red_damage");
        yield return new WaitForSeconds(1);
        // add to score
        Score.Instance.AddScore(100);
        Destroy(gameObject);
    }

    // this doesn't actually work, the enemies were going to be able to fix their surrounding blocks
    private bool CheckBlocks()
    {
        for(int i = 0; i < _blocks.Length; i++) { 
            if(_blocks[i]._countHits > 0)
            {
                // fix this block
                return true;
            }
        }

        return false;

    }

    // also doesn't work yet
    private IEnumerator FixBlock(Block block)
    { 
        while (_timer < 25f)
        {
            // play animation 
            yield return new WaitForEndOfFrame();
        }
        // block hit counter --
    }

    private IEnumerator Patrol()
    {
        while (!_isDead) {
            // lerp from point 1 to point2
            _timer = 0;
            while(_timer < _duration) {
                _animator.Play("red_walk");
                if (_isDead)    // had to set a variable to break out of while loop
                {  
                    break;
                }
                transform.position = Vector2.Lerp(new Vector2(_points[0].position.x, transform.position.y), new Vector2(_points[1].position.x, transform.position.y), _timer / _duration);
                _timer += Time.deltaTime;
                yield return null;
            }
            if (_isDead) {  // cheating to make it work
                break;
            }
            // make sure enemy fully at point2
            transform.position = new Vector2(_points[1].position.x, transform.position.y);
            _animator.Play("red_idle");
            _direction = !_direction;
            GetComponent<SpriteRenderer>().flipX = _direction;

            // wait for 3 seconds
            yield return new WaitForSeconds(2f);
            if (_isDead)
            {  // cheating to make it work
                break;
            }

            // lerp from point2 to point1
            _timer = 0;
            while (_timer < _duration)
            {
                _animator.Play("red_walk");
                if (_isDead)
                {  // cheating to make it work
                    break;
                }
                transform.position = Vector2.Lerp(new Vector2(_points[1].position.x, transform.position.y), new Vector2(_points[0].position.x, transform.position.y), _timer / _duration);
                _timer += Time.deltaTime;
                yield return null;
            }
            if (_isDead)
            {  // cheating to make it work
                break;
            }

            // make sure enemy fully at point1
            transform.position = new Vector2(_points[0].position.x, transform.position.y);
            _animator.Play("red_idle");
            _direction = !_direction;
            GetComponent<SpriteRenderer>().flipX = _direction;

            // wait for 3 seconds
            yield return new WaitForSeconds(2f);

        }

    }


}
