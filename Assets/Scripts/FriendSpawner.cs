﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendSpawner : MonoBehaviour
{
    [SerializeField] private bool _flipX;
    [SerializeField] private FriendlyAI[] _friends;
    private int _index;

    private void Start()
    {
        // find what dino index needs to be spawned
        _index = FindObjectOfType<SetFriendType>()._dinoIndex;
        // spawn friend at spawner position
        FriendlyAI friend = Instantiate(_friends[_index], new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
        // if sprite needs to be flipped, flip X
        friend.GetComponent<SpriteRenderer>().flipX = _flipX;
    }
}
