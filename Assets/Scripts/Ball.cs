﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // paddle reference so ball can lock to it
    [SerializeField] private Paddle _paddle;

    // get reference to rigidbody
    public Rigidbody2D _rigidbody;

    // offset from ball to paddle
    private Vector2 _offsetToPaddle;
    // check if game has started
    public bool _gameStart = false;

    public int lives = 3;

    private void Start()
    {
        // get the offset between ball and paddle
        _offsetToPaddle = transform.position - _paddle.transform.position;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // user can reset ball back to paddle
        if(Input.GetButtonDown("Jump") && lives != 0){
            _gameStart = false;
        }

        if(lives <= 0){
            FindObjectOfType<SceneLoader>().LoadGameOver();
        }

        if(!_gameStart){
            // paddle is locked to paddle
            LockedBall();
            // click to launch ball
            LaunchBall();
        }
    }

    private void LockedBall(){
        // sets ball position to paddle + offset
        transform.position = (Vector2)_paddle.transform.position + _offsetToPaddle;
    }

    private void LaunchBall(){
        if(Input.GetMouseButtonDown(0)){
             // add up force to shoot ball
             _gameStart = true;
             _rigidbody.velocity = new Vector2(2f, 15f);  
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(_gameStart){
            // change the velocity a random amount for each collision
            Vector2 velocityChange = new Vector2(Random.Range(0,.2f), Random.Range(0,.2f));
            _rigidbody.velocity += velocityChange;
        }
    }

}
