﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    // enemy animator
    private Animator _thisAnimator;
    private void Start()
    {
        _thisAnimator = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if hit by the ball or superball
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Superball")
        {
            StartCoroutine(Death());    // death coroutine
        }
    }

    private IEnumerator Death()
    {
        // play damage animation for 1 second
        _thisAnimator.Play("red_damage");
        yield return new WaitForSeconds(1);
        // add to score
        Score.Instance.AddScore(100);
        // destroy enemy
        Destroy(gameObject);
    }
}
