﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private void Awake()
    {   
        // make sure object is singleton
        Singleton();
    }

    private void Singleton()
    {
        // only want one music manager in game at all times
        int numberObjs = FindObjectsOfType<MusicManager>().Length;
        if (numberObjs > 1)
        {
            gameObject.SetActive(false);    // if too many objects, destroy object
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
