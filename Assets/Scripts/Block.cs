﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    // sprites to change out when block hit
    [SerializeField] private Sprite[] _hitSprites;

    // count number of hits
    public int _countHits;

    private void OnCollisionEnter2D(Collision2D other)
    {
        // don't register character collisions as hurting block
        if(other.gameObject.layer == LayerMask.NameToLayer("Characters")){
            return;
        }
        _countHits++;   // increment hit count
        if(_countHits >= _hitSprites.Length + 1){
            // destroy the block
            Score.Instance.AddScore(50);
            Destroy(gameObject);
        }
        else{
            // change sprite 
            if(_hitSprites[_countHits - 1] != null){ // check if sprite isn't missing
                GetComponent<SpriteRenderer>().sprite = _hitSprites[_countHits - 1];
            }
        }
    }
}
